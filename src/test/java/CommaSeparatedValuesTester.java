import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;

import static org.assertj.core.api.BDDAssertions.then;

public class CommaSeparatedValuesTester {
    protected CommaSeparatedValues csv;
    protected LinkedHashMap row1;
    protected LinkedHashMap row2;

    public static class When_Valid_No_Order_Is_Selected extends CommaSeparatedValuesTester {
        protected String noorderExpected;

        @Test
        public void it_should_return_proper_string() {
            csv.addRow(row1);
            csv.addRow(row2);
            then(csv.toString()).isEqualTo(noorderExpected);
        }

        @Before
        public void setup_context() {
            csv = new CommaSeparatedValues();

            row1 = new LinkedHashMap();
            row1.put("string1", "Leonardo");
            row1.put("string2", "Donatello");
            row1.put("int1", "3");
            row1.put("boolean1", "true");

            row2 = new LinkedHashMap();
            row2.put("string1", "Michelangelo");
            row2.put("string2", "Raphael");
            row2.put("int2", "3");
            row2.put("boolean2", "false");

            noorderExpected = "string1,string2,int1,boolean1,int2,boolean2\n" +
                              "Leonardo,Donatello,3,true,,\n" +
                              "Michelangelo,Raphael,,,3,false\n";
        }
    }

    public static class When_Valid_Inorder_Is_Selected extends CommaSeparatedValuesTester {
        protected CommaSeparatedValues csv;
        protected String toStringed;
        protected LinkedHashMap row1;
        protected LinkedHashMap row2;

        @Test
        public void it_should_return_proper_inorder_csv_string() {
            csv.addRow(row1);
            csv.addRow(row2);
            then(csv.toString()).isEqualTo(toStringed);
        }

        @Before
        public void setup_context() {
            csv = new CommaSeparatedValues(CommaSeparatedValues.Order.Inorder);

            row1 = new LinkedHashMap();
            row1.put("string1", "Leonardo");
            row1.put("string2", "Donatello");
            row1.put("int1", "3");
            row1.put("boolean1", "true");

            row2 = new LinkedHashMap();
            row2.put("string1", "Michelangelo");
            row2.put("string2", "Raphael");
            row2.put("int2", "3");
            row2.put("boolean2", "false");

            toStringed = "boolean1,boolean2,int1,int2,string1,string2\n" +
                         "true,,3,,Leonardo,Donatello\n" +
                         ",false,,3,Michelangelo,Raphael\n";
        }
    }

    public static class When_Valid_Insertion_Is_Selected extends CommaSeparatedValuesTester {
        protected CommaSeparatedValues csv;
        protected String inorderExpected;
        protected LinkedHashMap row1;
        protected LinkedHashMap row2;

        @Test
        public void it_should_return_proper_insertion_csv_string() {
            csv.addRow(row1);
            csv.addRow(row2);
            then(csv.toString()).isEqualTo(inorderExpected);
        }

        @Before
        public void setup_context() {
            csv = new CommaSeparatedValues(CommaSeparatedValues.Order.Insertion);

            row1 = new LinkedHashMap();
            row1.put("z", "Leonardo");
            row1.put("y", "Donatello");
            row1.put("x", "3");
            row1.put("w", "true");

            row2 = new LinkedHashMap();
            row2.put("z", "Michelangelo");
            row2.put("y", "Raphael");
            row2.put("a", "3");
            row2.put("b", "false");

            inorderExpected = "z,y,x,w,a,b\n" +
                              "Leonardo,Donatello,3,true,,\n" +
                              "Michelangelo,Raphael,,,3,false\n";
        }
    }
}
