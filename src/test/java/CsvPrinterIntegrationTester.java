import com.fasterxml.jackson.databind.ObjectMapper;

import org.assertj.core.api.BDDAssertions;
import org.junit.Test;

public class CsvPrinterIntegrationTester {
    protected String actual;
    protected String expected;
    protected String testDestinationFile = "testJson";

    @Test
    public void it_should_import_the_json_and_return_valid_csv_formatted_string() {
        CsvPrinter printer = new CsvPrinter(new CsvFactory(), new FileToJsonImporter(new ObjectMapper()));

        expected = "id,first_name,last_name,middle_initial,birth_year,anonymous_user,crm_id,profession,e_equals_mc_squared\n" +
                   "1,Jane,Doe,,,,,,\n" +
                   "2,John,Public,Q,1971,,,,\n" +
                   "3,,,,,true,abc123,,\n" +
                   "4,Albert,Einstein,,1879,,,Scientist,true\n";

        actual = printer.importFile(testDestinationFile);
        BDDAssertions.then(actual).isEqualTo(expected);
    }
}
