import com.fasterxml.jackson.databind.JsonNode;

import org.assertj.core.api.BDDAssertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class CsvPrinterTester {
    protected CsvPrinter printer;
    protected CsvFactory factory;
    protected FileToJsonImporter importer;
    protected String actual;
    protected String input = "My input";

    @Before
    public void setup() {
        factory = mock(CsvFactory.class);
        importer = mock(FileToJsonImporter.class);
        printer = new CsvPrinter(factory, importer);
    }

    public static class When_Valid_Valid_String_Should_Be_Returned extends CsvPrinterTester {
        protected String expectedSuccess;

        @Test
        public void it_should_read_json_and_create_cvs_and_return_string_representation() {
            BDDAssertions.then(actual).isEqualTo(expectedSuccess);
        }

        @Before
        public void setup_context() {
            expectedSuccess = "My expected string output";

            JsonNode[] nodes = new JsonNode[0];
            Optional<JsonNode[]> optionalNodes = Optional.of(nodes);
            given(importer.read(input)).willReturn(optionalNodes);

            CommaSeparatedValues csv = mock(CommaSeparatedValues.class);
            Optional<CommaSeparatedValues> optionalCsv = Optional.of(csv);
            given(factory.create(nodes)).willReturn(optionalCsv);

            given(csv.toString()).willReturn(expectedSuccess);

            actual = printer.importFile(input);
        }
    }

    public static class When_Valid_Failed_To_Import_Json_File extends CsvPrinterTester {
        protected String expectedError;

        @Test
        public void it_should_return_error_message() {
            BDDAssertions.then(actual).isEqualTo(expectedError);
        }

        @Before
        public void setup_context() {
            expectedError = "Unable to find valid json";

            Optional<JsonNode[]> optionalNodes = Optional.empty();
            given(importer.read(input)).willReturn(optionalNodes);

            actual = printer.importFile(input);
        }
    }

    public static class When_Valid_Failed_To_Create_Csv extends CsvPrinterTester {
        protected String expectedError;

        @Test
        public void it_should_return_error_message() {
            BDDAssertions.then(actual).isEqualTo(expectedError);
        }

        @Before
        public void setup_context() {
            expectedError = "Unable to create csv from json";

            JsonNode[] nodes = new JsonNode[0];
            Optional<JsonNode[]> optionalNodes = Optional.of(nodes);
            given(importer.read(input)).willReturn(optionalNodes);

            Optional<CommaSeparatedValues> optionalCsv = Optional.empty();
            given(factory.create(nodes)).willReturn(optionalCsv);

            actual = printer.importFile(input);
        }
    }
}