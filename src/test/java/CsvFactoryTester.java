import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.assertj.core.api.BDDAssertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvFactoryTester {
    public static class When_Passing_Valid_Json_Nodes extends CsvFactory {
        protected List<String> expectedHeaders;
        protected Map<String, String> expectedFirstRow;
        protected Map<String, String> expectedSecondRow;
        protected JsonNode[] input;

        @Test
        public void it_should_map_first_json_node_to_first_row() {
            CsvFactory factory = new CsvFactory();
            CommaSeparatedValues actual = factory.create(input).get();
            BDDAssertions.then(actual.rows.size()).isEqualTo(2);
            BDDAssertions.then(actual.rows.get(0)).containsAllEntriesOf(expectedFirstRow);
        }

        @Test
        public void it_should_map_first_json_node_to_second_row() {
            CsvFactory factory = new CsvFactory();
            CommaSeparatedValues actual = factory.create(input).get();
            BDDAssertions.then(actual.rows.size()).isEqualTo(2);
            BDDAssertions.then(actual.rows.get(1)).containsAllEntriesOf(expectedSecondRow);
        }

        @Test
        public void it_should_map_json_nodes_to_correct_headers() {
            CsvFactory factory = new CsvFactory();
            CommaSeparatedValues actual = factory.create(input).get();
            BDDAssertions.then(expectedHeaders).containsOnlyElementsOf(actual.headers);
        }

        @Before
        public void setup_context() {

            ObjectMapper objectMapper = new ObjectMapper();

            input = new JsonNode[2];

            TestObject1 object1 = new TestObject1();
            input[0] = objectMapper.valueToTree(object1);
            TestObject2 object2 = new TestObject2();
            input[1] = objectMapper.valueToTree(object2);

            expectedHeaders = Arrays.asList("string1", "string2", "int1", "boolean1", "int2", "boolean2");

            expectedFirstRow = new HashMap();
            expectedFirstRow.put("string1", "Leonardo");
            expectedFirstRow.put("string2", "Donatello");
            expectedFirstRow.put("int1", "3");
            expectedFirstRow.put("boolean1", "true");

            expectedSecondRow = new HashMap();
            expectedSecondRow.put("string1", "Michelangelo");
            expectedSecondRow.put("string2", "Raphael");
            expectedSecondRow.put("int2", "3");
            expectedSecondRow.put("boolean2", "false");
        }

        public class TestObject1 {
            public String string1 = "Leonardo";
            public String string2 = "Donatello";
            public int int1 = 3;
            public boolean boolean1 = true;
        }

        public class TestObject2 {
            public String string1 = "Michelangelo";
            public String string2 = "Raphael";
            public int int2 = 3;
            public boolean boolean2 = false;
        }
    }

    public static class When_Passing_Invalid_Json_Nodes extends CsvFactory {
        @Test
        public void it_should_return_empty_optional_when_null_json_nodes_are_passed_as_inputs() {
            CsvFactory factory = new CsvFactory();
            BDDAssertions.then(factory.create(null).isPresent()).isFalse();
        }
    }
}
