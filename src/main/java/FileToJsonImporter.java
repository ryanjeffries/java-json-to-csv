import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.assertj.core.util.Strings;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class FileToJsonImporter {
    protected ObjectMapper objectMapper;

    public FileToJsonImporter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public Optional<JsonNode[]> read(String file) {

        String filePath = file;
        try {
            filePath = getClass().getResource(file).getPath();
        } catch (Exception e) {
            //Ignored Exception since we will try to find entire path rather than relative
        }

        if (Strings.isNullOrEmpty(file))
            return Optional.empty();

        try {
            return Optional.ofNullable(objectMapper.readValue(new File(filePath), JsonNode[].class));
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}