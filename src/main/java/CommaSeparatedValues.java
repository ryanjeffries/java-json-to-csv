import com.google.common.base.Joiner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class CommaSeparatedValues {
    protected Set<String> headers;
    protected List<Map<String, String>> rows;

    public CommaSeparatedValues(Order order) {
        switch (order) {
            case Inorder:
                headers = new TreeSet();
                break;
            case Insertion:
                headers = new LinkedHashSet();
                break;
            default:
                headers = new HashSet();
        }
        rows = new ArrayList();
    }

    public CommaSeparatedValues() {
        this(Order.Insertion);
    }

    public void addRow(Map<String, String> row) {
        row.keySet().stream().forEach(key -> headers.add(key));
        rows.add(row);
    }

    @Override
    public String toString() {

        StringBuilder cvs = new StringBuilder();

        cvs.append(Joiner.on(",").join(headers) + "\n");

        rows.stream().forEach(row -> {
            headers.stream().forEach(header -> {
                if (row.containsKey(header))
                    cvs.append(row.get(header));
                cvs.append(",");
            });
            cvs.replace(cvs.length() - 1, cvs.length(), "\n");
        });

        return cvs.toString();
    }

    public enum Order {
        Inorder, Insertion, Noorder;
    }
}