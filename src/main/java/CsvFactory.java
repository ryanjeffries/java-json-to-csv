import com.fasterxml.jackson.databind.JsonNode;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class CsvFactory {
    public Optional<CommaSeparatedValues> create(JsonNode[] jsonNodes) {
        if (jsonNodes == null)
            return Optional.empty();

        CommaSeparatedValues cvs = new CommaSeparatedValues();
        Arrays.stream(jsonNodes).forEach(jsonNode -> {
            Map<String, String> rowToAdd = new LinkedHashMap<>();
            jsonNode.fields().forEachRemaining(field -> rowToAdd.put(field.getKey(), field.getValue().asText()));
            cvs.addRow(rowToAdd);
        });
        return Optional.of(cvs);
    }
}