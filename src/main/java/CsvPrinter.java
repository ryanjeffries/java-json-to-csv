import com.fasterxml.jackson.databind.JsonNode;

import java.util.Optional;

public class CsvPrinter {
    protected FileToJsonImporter fileToJsonImporter;
    protected CsvFactory csvFactory;

    public CsvPrinter(CsvFactory csvFactory, FileToJsonImporter fileToJsonImporter) {
        this.csvFactory = csvFactory;
        this.fileToJsonImporter = fileToJsonImporter;
    }

    public String importFile(String json) {
        Optional<JsonNode[]> jsonNodes = fileToJsonImporter.read(json);
        if (jsonNodes.isPresent()) {
            Optional<CommaSeparatedValues> csv = csvFactory.create(jsonNodes.get());
            if (csv.isPresent())
                return csv.get().toString();
            return "Unable to create csv from json";
        }
        return "Unable to find valid json";
    }

    public void print(String json) {
        System.out.println(importFile(json));
    }
}
